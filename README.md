# Training and content

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dsbowen%2Ftraining-content-study/master?urlpath=lab)

This repository contains data cleaning and analysis files for the training and context study.

## Environment

I use python-3.9.2 environment. Install the requirements with:

```bash
$ pip install -r requirements.txt
```

## Data

To avoid including PII, I removed the data when I made this repository public. To replicate my analyses, email me at dsbowen@wharton.upenn.edu or email Prof. Barb Mellers. We will send you a file named `FINAL SET.xlsx`. Put this in a folder named `data/raw`. Run `notebooks/clean.ipynb` to create a clean data file (stored in `data/clean/data.csv`). This is the file you should use for your analyses.

## Folder structure

`data/raw/` contains the "semi-cleaned" data file provided by Barb Mellers. Ideally, this repository should contain the R file used in the initial cleaning.

`data/clean/` contains the fully cleaned data file. You can generate this file by running `notebooks/clean.ipynb`.

`notebooks/` contains cleaning and analysis notebooks.

- `clean.ipynb` cleans the data
- `analyze.ipynb` runs exploratory analyses to look for main effects, conditional effects, and interaction effects by time-series type or "model". It generates 370 regression tables in total which are stored in `reports/`.
- `make_decision_trees.ipynb` is an alternative exploratory analysis. It trains exploratory decision trees to look for main, conditional, and interaction effects which may be more interpretable and statistically well-founded.
- `make_plots.ipynb` creates point plots showing the effect of conditions (context/content and training) on forecasting performance.

## Notes

"Context" and "content" are used interchangeably here. Some researchers on our team prefer "context", others prefer "content".
